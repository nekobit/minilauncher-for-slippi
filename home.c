#include <stdio.h>
#include <cjson/cJSON.h>
#include <stdint.h>
#include "replay.h"
#include "http.h"

Evas_Object* tab_home = NULL;
static Evas_Object* tab_home_content = NULL;
char const* home_url = "https://api.github.com/repos/project-slippi/Ishiiruka/releases";

	
static Eina_Bool
releases_result(struct memory_chunk* dd, void* data, void *elm_data EINA_UNUSED, int type EINA_UNUSED, void *event_info)
{
	Evas_Object* parent = data;
	Ecore_Con_Event_Url_Complete* ev = event_info;
	cJSON* json = cJSON_ParseWithLength(dd->data, dd->size);
	if (!json)
	{
		fprintf(stderr, "Something happened.\n");
		return EINA_FALSE;
	}
	
	// Clear out
	elm_obj_box_clear(parent);
	
	cJSON* release = NULL;
	//cJSON_ArrayForEach(release, json)
	int i = 0;
	for (cJSON* c = json->child; c->next != NULL; (++i, c = c->next))
	{
		if (i == 6)
		{
			Evas_Object* notice = elm_label_add(parent);
			elm_object_text_set(notice, "Older releases are collapsed, click to expand");
			elm_box_pack_end(parent, notice);
			evas_object_show(notice);
		}
		Evas_Object* release_fr = elm_frame_add(parent);
		// Size
		evas_object_size_hint_weight_set(release_fr, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
		evas_object_size_hint_align_set(release_fr, EVAS_HINT_FILL, EVAS_HINT_FILL);
		// END Size
		elm_frame_autocollapse_set(release_fr, EINA_TRUE);
		if (i > 5)
			elm_frame_collapse_set(release_fr, EINA_TRUE);
		elm_box_pack_end(parent, release_fr);
		cJSON* title = cJSON_GetObjectItemCaseSensitive(c, "name");
		if (!title) // (((Rate limit))) and/or whatever bullshit crops up.
			return 1;
		elm_object_text_set(release_fr, title->valuestring);
		evas_object_show(release_fr);
		
		Evas_Object* fr_box = elm_box_add(release_fr);
		elm_object_content_set(release_fr, fr_box);
		evas_object_show(fr_box);
		
		Evas_Object* content = elm_entry_add(fr_box);
		elm_box_pack_end(fr_box, content);
		elm_entry_editable_set(content, EINA_FALSE);
		evas_object_size_hint_weight_set(content, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
		evas_object_size_hint_align_set(content, EVAS_HINT_FILL, EVAS_HINT_FILL);
		
		/* Get body_html and manipulate it */
		cJSON* body = cJSON_GetObjectItemCaseSensitive(c, "body_html");
		if (!body)
			return 1;
		Eina_Strbuf* body_fmt = eina_strbuf_manage_new(body->valuestring);
		eina_strbuf_replace_all(body_fmt, "\n", "<br>");
		eina_strbuf_replace_all(body_fmt, "<h1>", "<title>");
		eina_strbuf_replace_all(body_fmt, "</h1>", "</>");
		eina_strbuf_replace_all(body_fmt, "<h2>", "<subtitle>");
		eina_strbuf_replace_all(body_fmt, "</h2>", "</>");
		eina_strbuf_replace_all(body_fmt, "<h3>", "<heading>");
		eina_strbuf_replace_all(body_fmt, "</h3>", "</>");
		eina_strbuf_replace_all(body_fmt, "<li>", "<li> - ");
		eina_strbuf_trim(body_fmt);
		
		
		elm_object_text_set(content, eina_strbuf_string_get(body_fmt));
		evas_object_show(content);
		
		free(eina_strbuf_release(body_fmt));
		
		//elm_box_pack_end(tab_home_box, fr_box);
	}
	return EINA_TRUE;
}

void
_tab_home_make_da_damn_request(Evas_Object* parent)
{
	//ecore_con_init();
	//ecore_con_url_init();
	Ecore_Con_Url* ec_url = ecore_con_url_custom_new(home_url, "GET");
	ecore_con_url_data_set(ec_url, memory_chunk_alloc(releases_result, parent));
	ecore_event_handler_add(ECORE_CON_EVENT_URL_DATA, memory_chunk_data, NULL);
	ecore_event_handler_add(ECORE_CON_EVENT_URL_COMPLETE, memory_chunk_result, NULL);
	ecore_con_url_additional_header_add(ec_url, "User-Agent", ":^)'); DROP TABLE issues; --");
	ecore_con_url_additional_header_add(ec_url, "Accept", "application/vnd.github.html");
	ecore_con_url_get(ec_url);
}

Evas_Object*
tab_home_setup(Evas_Object* parent)
{
	tab_home = elm_box_add(parent); // Scroller
	evas_object_size_hint_weight_set(tab_home, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(tab_home, EVAS_HINT_FILL, EVAS_HINT_FILL);
	
	Evas_Object* tab_home_scr = elm_scroller_add(tab_home);
	evas_object_size_hint_weight_set(tab_home_scr, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(tab_home_scr, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_box_pack_end(tab_home, tab_home_scr);
	
	tab_home_content = elm_box_add(tab_home_scr);
	evas_object_size_hint_weight_set(tab_home_content, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(tab_home_content, EVAS_HINT_FILL, EVAS_HINT_FILL);
	
	elm_object_content_set(tab_home_scr, tab_home_content);
	
	evas_object_show(tab_home_content);
	evas_object_show(tab_home_scr);
	evas_object_show(tab_home);
	
   	//_tab_home_make_da_damn_request(tab_home_content);
	
	// Add progress bar for noa
	Evas_Object* speen = elm_progressbar_add(tab_home_content);
	elm_object_style_set(speen, "wheel");
	elm_progressbar_pulse_set(speen, EINA_TRUE);
	elm_progressbar_pulse(speen, EINA_TRUE);
	elm_box_pack_end(tab_home_content, speen);
	evas_object_show(speen);
	
   	return tab_home;
}
