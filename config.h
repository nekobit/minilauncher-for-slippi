#ifndef CONFIG_H
#define CONFIG_H

static int opt_mallocd = -1;
extern char* game_path;
extern char* dolphin_emu_file;
extern char* dolphin_replay_file;
extern char* start_gg_api;
#ifndef DATA_DIR
#define DATA_DIR "data/"
#endif

int
parse_config(char* file);

void
cleanup_config(void);

#endif /* CONFIG_H */
