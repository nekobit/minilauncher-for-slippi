/* A question worth asking: Why not make the GG stuff an actual library?
 *  The reason: start.gg is DOGSHIT, there are several API's, all of which are functionally
 *  different and broken in their own ways. You have no real clue when stuff is going
 *  to change, and some parts are _COMPLETELY_ undocumented. Some of the API's return
 *  up to 88kb (!!!) of data... it's so horrific and bade i do not want to... go into it.
 *
 * Alas, I have no motivation to make this its own API. There are also Python wrappers and
 *  such, which is good especially if you need statistics from matches.
 */

#ifndef GG_API_H
#define GG_API_H
#define EFL_BETA_API_SUPPORT
#include <Ecore.h>
#include <Elementary.h>

extern Evas_Object* tab_gg;

Evas_Object*
gg_create_view(Evas_Object* parent);

#endif /* GG_API_H */
