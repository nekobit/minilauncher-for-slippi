#include <stdio.h>
#include <Ecore.h>
#include <Elementary.h>
#include "config.h"

char* game_path = "SSBM.iso";
char* dolphin_emu_file = "slippi-netplay-dolphin";
char* dolphin_replay_file = "slippi-playback-dolphin";
char* start_gg_api = ":^)";

int
parse_config(char* file)
{
	FILE* CFG = fopen(file, "r");
	if (!CFG)
	{	
		perror("fopen");
		return 1;
	}
	
	int buf_len = 255;
	char buf[buf_len];
	char* rdpnt;
	for (int i = 0; fgets(buf, buf_len, CFG); ++i) {
		if ((rdpnt = strchr(buf, '\n')))
			*rdpnt = '\0';
		switch (i)
		{
		case 0: game_path = strdup(buf); break;
		case 1: dolphin_emu_file = strdup(buf); break;
		case 2: dolphin_replay_file = strdup(buf); break;
		case 3: start_gg_api = strdup(buf); break;
		}
    	++opt_mallocd;
    }
abort:	
	fclose(CFG);
	return 0;
}

void
cleanup_config(void)
{
	if (opt_mallocd >= 0) free(game_path);
	if (opt_mallocd >= 1) free(dolphin_emu_file);
	if (opt_mallocd >= 2) free(dolphin_replay_file);
	if (opt_mallocd >= 3) free(start_gg_api);
}
