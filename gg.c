#include <cjson/cJSON.h>
#include "gg.h"
#include "http.h"

// Static
#include "static/tourney_query.json.h"

struct _gg_login_info
{
	Evas_Object* username_evas;
	Evas_Object* password_evas;
	Evas_Object* hideme;
};

Evas_Object* tab_gg = NULL;
static Evas_Object* tab_gg_sidebar;
static Evas_Object* tab_gg_content;
extern char* start_gg_api;
//extern char* start_gg_token;

static Eina_Bool
_gg_tourneys_result(struct memory_chunk* dd, void* _data, void *elm_data EINA_UNUSED, int type EINA_UNUSED, void *event_info);
static void
_gg_set_token(Ecore_Con_Url* ec_url, char* token);

static int player_id;

static Eina_Bool
_gg_image_result(struct memory_chunk* dd, void* _data, void *elm_data EINA_UNUSED, int type EINA_UNUSED, void *event_info)
{
	printf("%d\n", dd->size);
	Evas_Object* btn = _data;
	Evas_Object* icon = elm_icon_add(btn);
	elm_image_memfile_set(icon, dd->data, dd->size, NULL, NULL);
	//elm_image_resizable_set(icon, EINA_FALSE, EINA_FALSE);
	elm_object_content_set(btn, icon);
	//evas_object_resize(icon, 40, 40);
	//efl_gfx_entity_scale_set(icon,
	evas_object_size_hint_min_set(icon, 45, 45);
	evas_object_size_hint_max_set(icon, 45, 45);
}

static void
make_text_popup(char* text)
{
	Evas_Object* lbl = elm_label_add(tab_gg),
	           * notify = elm_notify_add(tab_gg);
	elm_object_text_set(lbl, text);
	evas_object_size_hint_weight_set(notify, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	elm_notify_align_set(notify, 0.5, 1.0);
	elm_notify_timeout_set(notify, 5.0);
	elm_object_content_set(notify, lbl);
	evas_object_show(notify);
	evas_object_show(lbl);
}

static void
_gg_get_tourneys(char* token)
{
	Ecore_Con_Url* ec_url = ecore_con_url_custom_new("https://www.start.gg/api/-/gql", "POST");
	ecore_con_url_data_set(ec_url, memory_chunk_alloc(_gg_tourneys_result, NULL));
	//ecore_event_handler_add(ECORE_CON_EVENT_URL_DATA, memory_chunk_data, NULL);
	//ecore_event_handler_add(ECORE_CON_EVENT_URL_COMPLETE, memory_chunk_result, NULL);
	ecore_con_url_additional_header_add(ec_url, "User-Agent", ":^)'); DROP TABLE users; --");
	
	_gg_set_token(ec_url, token);
	// Fill info
	Eina_Strbuf* req = eina_strbuf_new();
	eina_strbuf_append_printf(req, data_tourney_query, player_id);
	// Need to remove real newlines
	eina_strbuf_replace_all(req, "\n", "\\n");
	// Need to remove the newline at the end or it's not valid.
	ecore_con_url_post(ec_url, eina_strbuf_string_get(req), eina_strbuf_length_get(req)-2, "application/json, application/json");
	free(eina_strbuf_release(req));
}

static void
_gg_sidebar_cb(void *_data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
	Evas_Object** data = _data;
	//update_tab(*data);
}

static Eina_Bool
_gg_login_result(struct memory_chunk* dd, void* _data, void *elm_data EINA_UNUSED, int type EINA_UNUSED, void *event_info)
{
	struct _gg_login_info* info = _data;
	Ecore_Con_Event_Url_Complete* ev = event_info;
	// BEGIN login result
	cJSON* json = cJSON_Parse(dd->data);
	cJSON* entities = cJSON_GetObjectItemCaseSensitive(json, "entities");
	cJSON* users = cJSON_GetObjectItemCaseSensitive(entities, "users");
	
	cJSON* jplayer_id = cJSON_GetObjectItemCaseSensitive(users, "id");
	if (jplayer_id)
		player_id = jplayer_id->valueint;
	// END
	
	// Error
	if (!player_id)
	{
		if (strstr(dd->data, "\"message\""))
		{
			cJSON* message = cJSON_GetObjectItemCaseSensitive(json, "message");
			make_text_popup(message->valuestring);
		}
		else {
			make_text_popup(dd->data);
		}
		return EINA_FALSE; // We're not logged in.
	}
	
	char* str;
	char* duped = NULL, *duped_data = NULL;
	const Eina_List *headers, *l;
	headers = ecore_con_url_response_headers_get(ev->url_con);
	EINA_LIST_FOREACH(headers, l, str)
	{
		char* res = strstr(str, "gg_session=");
		if (res)
		{
			// Is this required?
			duped_data = duped = strdup(res);
			
			duped += 11;
			char* end = strchr(duped, ';');
			if (end) *end = '\0';
			break;
		}
	}
	
	if (duped)
	{
		start_gg_api = strdup(duped);
		//free(duped);
		free(duped_data);
	}
	
	printf("start_gg_api: %s\n", duped);
	
	// TODO duped code...
	evas_object_del(info->hideme);
	_gg_get_tourneys(start_gg_api);
	
	return EINA_TRUE;
}

static void
_gg_login_box(void *_data, Evas_Object *obj EINA_UNUSED, void *event_info EINA_UNUSED)
{
	struct _gg_login_info* info = _data;
	char* username = elm_entry_entry_get(info->username_evas);
	char* password = elm_entry_entry_get(info->password_evas);
	
	if (!(username && password))
		return; // Show error?
	
	Ecore_Con_Url* ec_url = ecore_con_url_custom_new("https://www.start.gg/api/-/rest/user/login", "POST");
	ecore_con_url_data_set(ec_url, memory_chunk_alloc(_gg_login_result, info));
	//ecore_con_url_verbose_set(ec_url, EINA_TRUE);
	ecore_con_url_additional_header_add(ec_url, "User-Agent", ":^)'); DROP TABLE users; --");
	ecore_con_url_additional_header_add(ec_url, "x-web-source", "gg-web-gql-client, gg-web-rest");
	// Fill info
	
	cJSON* json = cJSON_CreateObject();
	{
	
		cJSON_AddItemToObject(json, "email", cJSON_CreateString(username));
		cJSON_AddItemToObject(json, "password", cJSON_CreateString(password));
		cJSON_AddItemToObject(json, "expand", cJSON_CreateArray());
		cJSON_AddItemToObject(json, "rememberMe", cJSON_CreateBool(cJSON_True));
		cJSON_AddItemToObject(json, "validationKey", cJSON_CreateString("LOGIN_userlogin")); // lolwut?
	}
	
	char* jsonstring = cJSON_Print(json);
	ecore_con_url_post(ec_url, jsonstring, strlen(jsonstring), "application/json, application/json");

	// Debated...
	//evas_object_del(info->hideme);
	
	//free(_data);
}


static void
_gg_set_token(Ecore_Con_Url* ec_url, char* token)
{
	Eina_Strbuf* sesh = eina_strbuf_new();
	eina_strbuf_append_printf(sesh, "gg_session=%s;", token);
	//Eina_Strbuf* bearer = eina_strbuf_new();
	//eina_strbuf_append_printf(bearer, "Bearer %s", start_gg_token);
	ecore_con_url_additional_header_add(ec_url, "Cookie", eina_strbuf_string_get(sesh));
	ecore_con_url_additional_header_add(ec_url, "x-web-source", "gg-web-gql-client, gg-web-rest");
	//ecore_con_url_additional_header_add(ec_url, "Authorization", eina_strbuf_string_get(bearer));
	free(eina_strbuf_release(sesh));
	//free(eina_strbuf_release(bearer));
}

static Eina_Bool
_gg_tourneys_result(struct memory_chunk* dd, void* _data, void *elm_data EINA_UNUSED, int type EINA_UNUSED, void *event_info)
{
	Ecore_Con_Event_Url_Complete* ev = event_info;

	cJSON* json = cJSON_Parse(dd->data);
	if (!json)
	{
		fprintf(stderr, "Something happened.\n");
		return EINA_FALSE;
	}
	
	cJSON* data = cJSON_GetObjectItemCaseSensitive(json->child, "data");
	cJSON* user = cJSON_GetObjectItemCaseSensitive(data, "user");
	cJSON* player = cJSON_GetObjectItemCaseSensitive(user, "player");
	cJSON* events = cJSON_GetObjectItemCaseSensitive(player, "events");
	cJSON* nodes = cJSON_GetObjectItemCaseSensitive(events, "nodes");
	
	//int i = 0;
	//for (cJSON* c = nodes->child; c->next != NULL; (++i, c = c->next))
	cJSON* c;
	cJSON_ArrayForEach(c, nodes)
	{
		cJSON* entity = cJSON_GetObjectItemCaseSensitive(c, "entity");
		cJSON* images = cJSON_GetObjectItemCaseSensitive(entity, "images");
		char* name = cJSON_GetObjectItemCaseSensitive(entity, "name")->valuestring;
		
		Evas_Object* that;
		that = elm_button_add(tab_gg_sidebar);
		evas_object_size_hint_min_set(that, 50, 50);
		evas_object_size_hint_max_set(that, 50, 50);
		
		if (images && images->child)
		{
			images = images->child;
			char* imgurl = cJSON_GetObjectItemCaseSensitive(images, "url")->valuestring;
			Ecore_Con_Url* ec_url = ecore_con_url_custom_new(imgurl, "GET");
			ecore_con_url_data_set(ec_url, memory_chunk_alloc(_gg_image_result, that));
			//ecore_event_handler_add(ECORE_CON_EVENT_URL_DATA, memory_chunk_data, NULL);
			//ecore_event_handler_add(ECORE_CON_EVENT_URL_COMPLETE, memory_chunk_result, NULL);
			ecore_con_url_additional_header_add(ec_url, "User-Agent", ":^)'); DROP TABLE users; --");
			ecore_con_url_get(ec_url);
		}
		else
		{  
			Eina_Strbuf* strshort = eina_strbuf_new();
			eina_strbuf_append_printf(strshort, "%c", toupper(name[0]));
			Evas_Object* lbl = elm_label_add(that);
			elm_object_scale_set(lbl, 2.0);
			elm_object_text_set(lbl, eina_strbuf_string_get(strshort));
			elm_object_content_set(that, lbl);
			evas_object_show(lbl);
			free(eina_strbuf_release(strshort));
		}
		
		
		elm_object_tooltip_text_set(that, name);
		elm_object_tooltip_orient_set(that, ELM_TOOLTIP_ORIENT_RIGHT);
		elm_box_pack_end(tab_gg_sidebar, that); evas_object_show(that);
	}
	
	return EINA_TRUE;
}

static Eina_Bool
_gg_scrape_result(struct memory_chunk* dd, void* data, void *elm_data EINA_UNUSED, int type EINA_UNUSED, void *event_info)
{
	if (!dd->data)
	{
		make_text_popup("Start.gg didn't load. Are you connected?");
		return EINA_FALSE;
	}
#define magiccode "script id=\"__NEXT_DATA__\" type=\"application/json\">"
	char* start_json = strstr(dd->data, magiccode);
	if (!start_json)
	{
		fprintf(stderr, "Something happened with start_json.\n");
		return EINA_FALSE; // Some error
	}
	start_json += sizeof(magiccode) - 1;
	
	char* end_json = strstr(start_json, "</script><script>");
	if (!end_json)
	{
		fprintf(stderr, "Something happened with end_json.\n");
		return EINA_FALSE;
	}
	*end_json = '\0';
#undef magiccode
	// Data required is scraped now!
	cJSON* json = cJSON_Parse(start_json);
	if (!json)
	{
		fprintf(stderr, "Something happened.\n");
		return EINA_FALSE;
	}
	
	cJSON* props = cJSON_GetObjectItemCaseSensitive(json, "props");
	cJSON* pageProps = cJSON_GetObjectItemCaseSensitive(props, "pageProps");
	cJSON* currentUserFetchSpec = cJSON_GetObjectItemCaseSensitive(
	                                  pageProps, "currentUserFetchSpec");
	cJSON* entities = cJSON_GetObjectItemCaseSensitive(currentUserFetchSpec, "entities");
	cJSON* users = cJSON_GetObjectItemCaseSensitive(entities, "users");
	
	cJSON* jplayer_id = cJSON_GetObjectItemCaseSensitive(users, "id");
	if (jplayer_id)
		player_id = jplayer_id->valueint;
	else
	{
		make_text_popup("Token expired. Please login again.");
		
		
		Evas_Object* popup = elm_popup_add(tab_gg);
		Evas_Object* username = elm_entry_add(popup),
		           * password = elm_entry_add(popup);
		elm_popup_scrollable_set(popup, EINA_TRUE);
		{
			{
				evas_object_size_hint_weight_set(username, EVAS_HINT_EXPAND, 0.0);
				evas_object_size_hint_weight_set(password, EVAS_HINT_EXPAND, 0.0);
				//evas_object_size_hint_align_set(
				elm_entry_single_line_set(username, EINA_TRUE);
				elm_entry_single_line_set(password, EINA_TRUE);
				elm_entry_password_set(password, EINA_TRUE);
				evas_object_show(username); evas_object_show(password);
			}
			Evas_Object* login_prompt = elm_box_add(popup),
			           * username_box = elm_box_add(popup),
			           * password_box = elm_box_add(popup);
			{
				elm_box_horizontal_set(username_box, EINA_TRUE);
				elm_box_horizontal_set(password_box, EINA_TRUE);
				Evas_Object* username_lbl = elm_label_add(popup),
				           * password_lbl = elm_label_add(popup);
				elm_object_text_set(username_lbl, "Email: ");
				elm_object_text_set(password_lbl, "Password: ");
				elm_box_pack_end(username_box, username_lbl);
				elm_box_pack_end(username_box, username);
				elm_box_pack_end(password_box, password_lbl);
				elm_box_pack_end(password_box, password);
				evas_object_show(username_lbl);
				evas_object_show(password_lbl);
				evas_object_show(username_box);
				evas_object_show(password_box);
			}
			evas_object_show(login_prompt);
			elm_box_pack_end(login_prompt, username_box);
			elm_box_pack_end(login_prompt, password_box);
			elm_object_content_set(popup, login_prompt);
			// popup title
			elm_object_part_text_set(popup, "title,text", "Start.gg Login Required");
		}

		// popup buttons
		Evas_Object* poproot = elm_box_add(popup);
		{
			Evas_Object* popbtn = elm_button_add(poproot);
			elm_object_text_set(popbtn, "Login");
			struct _gg_login_info* info = malloc(sizeof(struct _gg_login_info));
			info->username_evas = username;
			info->password_evas = password;
			info->hideme = popup;
			evas_object_smart_callback_add(popbtn, "clicked", _gg_login_box, info);
			elm_box_pack_end(poproot, popbtn);
			evas_object_show(popbtn);
		}
		elm_object_part_content_set(popup, "button1", poproot);
		//evas_object_smart_callback_add(btn, "clicked", _popup_close_cb, popup);

		// popup show should be called after adding all the contents and the buttons
		// of popup to set the focus into popup's contents correctly.
		evas_object_show(poproot);
		evas_object_show(popup);

		return EINA_FALSE;
	}
	
	free(dd);
	
	// Now attempt another request
	// RANT: There are two endpoints. api.start.gg/gql/alpha is the official, and it SUCKS
	_gg_get_tourneys(data);
	
	return EINA_FALSE;
#if 0
	int i = 0;
	for (cJSON* c = tournament->child; c->next != NULL; (++i, c = c->next))
	{
		printf("name: %s\n", cJSON_GetObjectItemCaseSensitive(c, "name")->valuestring);
	}
#endif
}

static void
_gg_scrape_homepage(char* token)
{
	if (!ecore_con_url_pipeline_get())
    	ecore_con_url_pipeline_set(EINA_TRUE);
	Ecore_Con_Url* ec_url = ecore_con_url_custom_new("https://www.start.gg/", "GET");
	ecore_con_url_data_set(ec_url, memory_chunk_alloc(_gg_scrape_result, token));
	ecore_event_handler_add(ECORE_CON_EVENT_URL_DATA, memory_chunk_data, NULL);
	ecore_event_handler_add(ECORE_CON_EVENT_URL_COMPLETE, memory_chunk_result, NULL);
	ecore_con_url_additional_header_add(ec_url, "User-Agent", ":^)'); DROP TABLE users; --");
	_gg_set_token(ec_url, token);
	ecore_con_url_get(ec_url);
}

Evas_Object*
gg_create_view(Evas_Object* parent)
{
	Evas_Object* tb_it, * that, * ic;
	tab_gg = elm_box_add(parent);
	elm_box_horizontal_set(tab_gg, EINA_TRUE);
	evas_object_size_hint_weight_set(tab_gg, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(tab_gg, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(tab_gg);
	
	// Wait.. thats not a toolbar!
	tab_gg_sidebar = elm_box_add(tab_gg);
	//elm_toolbar_horizontal_set(tab_gg_sidebar, EINA_FALSE);
	//elm_object_style_set(tab_gg_sidebar, "item_vertical");
	//elm_toolbar_homogeneous_set(tab_gg_sidebar, EINA_TRUE);
	//elm_toolbar_shrink_mode_set(tab_gg_sidebar, ELM_TOOLBAR_SHRINK_MENU);
	//evas_object_size_hint_weight_set(tab_gg_sidebar, 0.0, EVAS_HINT_EXPAND);
	//evas_object_size_hint_align_set(tab_gg_sidebar, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_size_hint_weight_set(tab_gg_sidebar, 0.0, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(tab_gg_sidebar, 0.5, 0.0);
	elm_box_pack_end(tab_gg, tab_gg_sidebar);
	
	that = elm_button_add(tab_gg_sidebar);
	//elm_object_text_set(that, "HO");
	ic = elm_icon_add(that);
	elm_icon_standard_set(ic, "home");
	evas_object_show(ic);
	elm_object_content_set(that, ic);
	elm_object_style_set(that, "anchor");
	evas_object_size_hint_min_set(that, 50, 30);
	elm_box_pack_end(tab_gg_sidebar, that); evas_object_show(that);
	that = elm_button_add(tab_gg_sidebar);
	//elm_object_text_set(that, "NOTIFS");
	ic = elm_icon_add(that);
	elm_icon_standard_set(ic, "mail-unread");
	evas_object_show(ic);
	elm_object_content_set(that, ic);
	elm_object_style_set(that, "anchor");
	evas_object_size_hint_min_set(that, 50, 30);
	elm_box_pack_end(tab_gg_sidebar, that); evas_object_show(that);
	
	// Fake slpit
	//tb_it = elm_toolbar_item_append(tab_gg_sidebar, "home", NULL, _gg_sidebar_cb, NULL);
	//elm_toolbar_item_priority_set(tb_it, 100);
	//tb_it = elm_toolbar_item_append(tab_gg_sidebar, "mail-unread", NULL, _gg_sidebar_cb, NULL);
	//elm_toolbar_item_priority_set(tb_it, 100);
	//-----
	//elm_toolbar_item_separator_set(
	//	elm_toolbar_item_append(tab_gg_sidebar, NULL, NULL, NULL, NULL), EINA_TRUE);
	_gg_scrape_homepage(start_gg_api);
	//-----
#if 0
	elm_toolbar_item_separator_set(
		elm_toolbar_item_append(tab_gg_sidebar, NULL, NULL, NULL, NULL), EINA_TRUE);
	tb_it = elm_toolbar_item_append(tab_gg_sidebar, "view-list-compact", "", _gg_sidebar_cb, NULL);
	elm_toolbar_item_priority_set(tb_it, 100);
#endif

	
	
	Evas_Object* tab_gg_scr = elm_scroller_add(tab_gg);
	evas_object_size_hint_weight_set(tab_gg_scr, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(tab_gg_scr, EVAS_HINT_FILL, EVAS_HINT_FILL);
	elm_box_pack_end(tab_gg, tab_gg_scr);
	evas_object_show(tab_gg_scr);
	
	tab_gg_content = elm_box_add(tab_gg_scr);
	evas_object_size_hint_weight_set(tab_gg_content, EVAS_HINT_EXPAND, EVAS_HINT_EXPAND);
	evas_object_size_hint_align_set(tab_gg_content, EVAS_HINT_FILL, EVAS_HINT_FILL);
	evas_object_show(tab_gg_content);
	
	elm_object_content_set(tab_gg_scr, tab_gg_content);
	evas_object_show(tab_gg_content);
	evas_object_show(tab_gg_scr);
	evas_object_show(tab_gg_sidebar);
	evas_object_show(tab_gg);
	
	return tab_gg;
}
