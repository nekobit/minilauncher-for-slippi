CC=cc
CFLAGS=--std=c99 -g `pkg-config --cflags efl ecore elementary libusb-1.0` `sdl2-config --cflags` 
LDFLAGS=`pkg-config --libs efl ecore elementary libusb-1.0` `sdl2-config --libs` -lcjson
OBJS=main.o replay.o home.o input.o http.o gg.o config.o
OBJS_GG=main_ministartgg.o http.o gg.o config.o
CHEAD=static/tourney_query.json
CHEAD_C=static/tourney_query.json.h

all: filec $(CHEAD_C) minilauncher4slippi

filec: file-to-c/main.o
	$(CC) -o filec file-to-c/main.o
	
$(CHEAD_C): $(CHEAD)
	./filec static/tourney_query.json data_tourney_query > $@

minilauncher4slippi: $(OBJS)
	$(CC) -o minilauncher4slippi $(OBJS) $(LDFLAGS)

ministartgg: $(OBJS_GG)
	$(CC) -o ministartgg $(OBJS_GG) $(LDFLAGS)
	
clean:
	rm -f minilauncher4slippi ministartgg filec *.o static/*.h
