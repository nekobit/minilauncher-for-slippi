#define EFL_BETA_API_SUPPORT
#include <Ecore.h>
#include <Ecore_Con.h>
#include <stdio.h>
#include "http.h"

struct memory_chunk*
memory_chunk_alloc(Eina_Bool (*callback)(struct memory_chunk* meta, void* data,
	                      void *elm_data EINA_UNUSED,
	                      int type EINA_UNUSED,
	                      void *event_info), void* _data)
{
	struct memory_chunk* data = calloc(1, sizeof(struct memory_chunk));
	if (!data)
	{
		perror("memory_chunk_alloc");
		exit(1);
	}
	data->callback = callback;
	data->arg = _data;
	
	return data;
}

Eina_Bool
memory_chunk_data(void *data EINA_UNUSED, int type EINA_UNUSED, void *event_info)
{
	Ecore_Con_Event_Url_Data* ev = event_info;
	struct memory_chunk* dd = ecore_con_url_data_get(ev->url_con);

	dd->data = realloc(dd->data, dd->size + ev->size + 1);
	
#ifndef NDEBUG
	printf("---------------DEBUG---------------\n");
	printf("%.*s\n", ev->size, ev->data);
	printf("-----------------------------------\n\n");
#endif
	
	memcpy(dd->data + dd->size, ev->data, ev->size);
	dd->size += ev->size;
	dd->data[dd->size] = 0;
	return EINA_TRUE;
}

Eina_Bool
memory_chunk_result(void *data, int type, void *event_info)
{
	Ecore_Con_Event_Url_Complete* ev = event_info;
	struct memory_chunk* dd = ecore_con_url_data_get(ev->url_con);
	
	return dd->callback(dd, dd->arg, data, type, event_info);
}
