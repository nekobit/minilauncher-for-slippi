#ifndef HTTP_H
#define HTTP_H
#include <Ecore.h>

struct memory_chunk {
	char* data;
	size_t size;
	Eina_Bool (*callback)(struct memory_chunk* meta,
	                      void* data,
	                      void *elm_data EINA_UNUSED,
	                      int type EINA_UNUSED,
	                      void *event_info);
	void* arg;
};

struct memory_chunk*
memory_chunk_alloc(Eina_Bool (*callback)(struct memory_chunk* meta, void* data,
	                      void *elm_data EINA_UNUSED,
	                      int type EINA_UNUSED,
	                      void *event_info), void* data);

Eina_Bool
memory_chunk_data(void *data EINA_UNUSED, int type EINA_UNUSED, void *event_info);

Eina_Bool
memory_chunk_result(void *data, int type, void *event_info);

#endif /* HTTP_H */
