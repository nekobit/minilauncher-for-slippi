#include <stdio.h>
#define EFL_BETA_API_SUPPORT
#include <Ecore.h>
#include <Elementary.h>
#include <libusb.h>
#include "gg.h"
#include "http.h"
#include "config.h"

EAPI_MAIN int
elm_main(int argc, char **argv)
{
	parse_config("minilauncher4slippi.cfg");
	
	Evas_Object* win = elm_win_util_standard_add("ministartgg", "Ministartgg");
	elm_policy_set(ELM_POLICY_QUIT, ELM_POLICY_QUIT_LAST_WINDOW_CLOSED);
	elm_win_autodel_set(win, EINA_TRUE);

	Evas_Object* main = gg_create_view(win);
	evas_object_show(main);
	elm_win_resize_object_add(win, main);

	evas_object_resize(win, 520 * elm_config_scale_get(),
    	                    300 * elm_config_scale_get());
	evas_object_show(win);
	elm_run();

	cleanup_config();
}
ELM_MAIN()
